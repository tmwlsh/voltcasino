(function ($) {

	$(document).ready(function(){

		$("div.hamb").click(function(){
			$(this).toggleClass("open");
		});

		$('.slider-module').slick({
			centerMode: true,
			centerPadding: '0px',
			slidesToShow: 1,
			adaptiveHeight: true,
		});

		$('.why-repeater-block').matchHeight();

		$("div.faq-row h4.faq-header").click(function(){
			if ( $(this).hasClass('active-faq-row') ){
				return;
			} else {
				$("div.faq-content").slideUp();
				$("div.faq-row h4.faq-header").removeClass("active-faq-row");
				$(this).addClass('active-faq-row');
				$(this).siblings("div.faq-content").slideDown();
			}
		});

	});

}(jQuery));
