<?php
/*
 * Module: Contact Section
 */

$shortcode = get_sub_field('shortcode');
?>

    <section class="module module-contact-section clearfix">
            <div class="container">
                    <div class="contact-content-half half">
                            <?php the_sub_field('contact_content'); ?>
                    </div>
                    <div class="contact-form-half half">
                            <?php echo do_shortcode($shortcode); ?>
                    </div>
            </div>
    </section><!-- .module-contact-section -->

<?php
