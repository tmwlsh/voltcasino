<?php
/*
 * Module: Slider
 */

$link = get_sub_field('slide_link');

?>

    <section class="module module-slider">
            <div class="slider-module">
                    <?php if( have_rows('slider_module_slides') ): ?>
                        <?php while ( have_rows('slider_module_slides') ) : the_row(); ?>

                                <div class="slide-container" style="background-image:url('<?php the_sub_field('slide_bg'); ?>');">
                                        <div class="container">
                                                <div class="single-slide-content">
                                                        <?php the_sub_field('slide_copy'); ?>
                                                        <a href="<?php $link['url']; ?>">hello<?php $link['title']; ?></a>
                                                </div>
                                        </div>
                                </div>

                        <?php endwhile; ?>
                    <?php endif; ?>
            </div>
    </section><!-- .module-slider -->

<?php
