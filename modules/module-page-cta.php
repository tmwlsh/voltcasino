<?php
/*
 * Module: Page CTA
 */

$text = get_sub_field('header_text');
$partners = get_sub_field('partners');
?>

    <section class="module module-page-cta">
            <div class="container">
                    <div class="page-cta-header" style="background-image:url('<?php the_sub_field('page_cta_bg'); ?>')">
                            <img src="<?php the_sub_field('page_cta_logo'); ?>" />
                    </div>
                    <div class="page-cta-content clearfix">
                            <div class="page-cta-half">
                                    <?php the_sub_field('left_text'); ?>
                            </div>
                            <div class="page-cta-half clearfix">
                                    <a class="signup" href="#">Sign Up</a>
                                    <a href="<?php the_sub_field('learn_more_link'); ?>">Learn More</a>
                            </div>
                    </div>
            </div>
    </section><!-- .module-page-cta -->

<?php
