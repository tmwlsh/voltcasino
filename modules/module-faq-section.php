<?php
/*
 * Module: Faq Section
 */


?>

    <section class="module module-faq-section">
            <div class="container">
                    <h3><?php the_sub_field('faq_title'); ?></h3>
                    <?php if( have_rows('faq_repeater') ): ?>
                        <?php while ( have_rows('faq_repeater') ) : the_row(); ?>
                                <div class="faq-row">
                                        <h4 class="faq-header"><?php the_sub_field('faq_header'); ?></h4>
                                        <div class="faq-content">
                                                <?php the_sub_field('faq_content'); ?>
                                        </div>
                                </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
            </div>
    </section><!-- .module-faq-section -->

<?php
