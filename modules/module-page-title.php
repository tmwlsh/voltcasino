<?php
/*
 * Module: Page Title
 */

$titleText = get_sub_field('title_text');
$titleBG = get_sub_field('title_background');
?>

    <section class="module module-page-title" style="background-image:url('<?php echo $titleBG; ?>');">
            <div class="container">
                    <h1><?php echo $titleText; ?></h1>
            </div>
    </section><!-- .module-page-title -->

<?php
