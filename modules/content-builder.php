<?php
/*
* Module: Content builder
*/

$acf_id = mbl_acf_id();
if( have_rows('content_builder_modules', $acf_id) ): ?>

        <section class="content-builder">
                <?php while ( have_rows('content_builder_modules', $acf_id) ) : the_row();

                        if( get_row_layout() == 'page_cta' ) {
                                get_template_part( 'modules/module', 'page-cta' );
                        } else if( get_row_layout() == 'slider' ) {
                                get_template_part( 'modules/module', 'slider' );
                        } else if( get_row_layout() == 'page_title' ) {
                                get_template_part( 'modules/module', 'page-title' );
                        } else if( get_row_layout() == 'page_content' ) {
                                get_template_part( 'modules/module', 'page-content' );
                        } else if( get_row_layout() == 'testimonial_repeater' ) {
                                get_template_part( 'modules/module', 'testimonial-repeater' );
                        } else if( get_row_layout() == 'why_affiliate' ) {
                                get_template_part( 'modules/module', 'why-affiliate' );
                        } else if( get_row_layout() == 'faq_section' ) {
                                get_template_part( 'modules/module', 'faq-section' );
                        } else if( get_row_layout() == 'contact_section' ) {
                                get_template_part( 'modules/module', 'contact-section' );
                        }

                endwhile; ?><!-- .content-builder -->
        </section>

<?php endif;
