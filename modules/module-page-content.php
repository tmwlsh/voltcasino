<?php
/*
 * Module: Page Content
 */

$pageContent = get_sub_field('page_content');
?>

    <section class="module module-page-content">
            <div class="container">
                    <?php echo $pageContent; ?>
            </div>
    </section><!-- .module-page-content -->

<?php
