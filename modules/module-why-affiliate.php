<?php
/*
 * Module: Why Become An Affiliate
 */
?>

        <section class="module module-why-affiliate">
                <div class="container">
                        <div class="why-affiliate-intro">
                                <?php the_sub_field('why_affiliate_intro'); ?>
                        </div>
                        <?php if( have_rows('why_affiliate_repeater') ): ?>
                                <div class="why-repeater-container clearfix">
                                        <?php while ( have_rows('why_affiliate_repeater') ) : the_row(); ?>
                                                <div class="why-repeater-block">
                                                        <img src="<?php the_sub_field('why_affiliate_icon'); ?>" />
                                                        <div class="why-affiliate-text">
                                                                <?php the_sub_field('why_affiliate_text'); ?>
                                                        </div>
                                                </div>
                                        <?php endwhile; ?>
                                </div>
                        <?php endif; ?>
                </div>
        </section><!-- .module-why-affiliate -->

<?php
