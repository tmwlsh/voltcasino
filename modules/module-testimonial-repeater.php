<?php
/*
 * Module: Testimonial Repeater
 */

$link = get_sub_field('slide_link');
$introText = get_sub_field('testimonials_intro_text');

?>

    <section class="module module-testimonial-repeater">
            <div class="container">
                    <?php if( $introText ) { ?>
                            <div class="testimonial-intro">
                                    <?php echo $introText; ?>
                            </div>
                    <?php } ?>

                    <?php if( have_rows('testimonials_repeater') ): ?>
                        <?php while ( have_rows('testimonials_repeater') ) : the_row(); ?>

                                <div class="testimonial-row clearfix">
                                        <div class="testimonial-image" style="background-image:url('<?php the_sub_field('testimonial_image'); ?>');"></div>
                                        <div class="testimonial-content">
                                                <?php the_sub_field('testimonial_content'); ?>
                                        </div>
                                </div>

                        <?php endwhile; ?>
                    <?php endif; ?>
            </div>
    </section><!-- .module-testimonial-repeater -->

<?php
