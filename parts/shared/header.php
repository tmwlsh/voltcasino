<header class="clearfix">
	<div class="container">
		<a href="<?php echo site_url(); ?>"><img src="<?php the_field('site_logo', 'options'); ?>" /></a>
		<nav>
			<ul>
				<li><a class="login" href="#">Login</a></li>
				<li><a href="#">Sign Up</a></li>
			</ul>
		</nav>
	</div>
</header>
<nav class="header-nav clearfix">
	<div class="container">
		<?php wp_nav_menu( array( 'container_class' => 'header-nav', 'theme_location' => 'header-menu' ) ); ?>
	</div>
	<div class="hamb">
		<div class="line"></div>
		<div class="line"></div>
		<div class="line"></div>
	</div>	
</nav>
